#!/bin/sh

DBNAME=$1

rm -f /root/_db/latest/$DBNAME.sql.gz

dt=$(date +'%Y-%m-%d')

##
# Mysql dumps from main databases with gzip
mysqldump -uroot $DBNAME | gzip > /root/_db/latest/$DBNAME-$dt.sql.gz